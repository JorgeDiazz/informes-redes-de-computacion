package Servidor;

import java.io.*;//Importamos la libreria de java io 
import java.net.*;//importamos la libreria java net

public class Servidor {

	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = new ServerSocket(9999);//Creamos un socket del servidor y definimos que escuche peticiones al puerto 9999
		String ultimaUrl = null;//Lo utilizaremos para almacenar la ultima pagina existente requerida 

		while (true) {//Ciclo infinito que asegurara que siempre este a la escucha el socket del servidor
			System.out.println("Esperando peticion en puerto 9999...");//Impresion de mensaje en consola
			Socket socketEntrada = serverSocket.accept();//Aceptamos las conexiones entrantes

			InputStreamReader flujoEntrada = new InputStreamReader(socketEntrada.getInputStream()); // Se consigue el canal de entrada
			BufferedReader in = new BufferedReader(flujoEntrada);//Le pasamos el flujo de entrada para poder observar su contenido como una cadena de texto

			try {				
				String linea = in.readLine();///*Leemos la primer linea del flujo de entrada enviado por el navegador
				String url = linea.substring(linea.indexOf("/") +1,linea.lastIndexOf(" "));//Seleccionamos desde el indice del "/" +1 hasta el ultimo espacio de la linea leida
				
				String ipCliente = socketEntrada.getInetAddress().getHostAddress();//Obtenemos la direccion ip del cliente que esta requiriendo una pagina utilizando la informacion del socket
				System.out.println("Pagina requerida->" + url + ", ip cliente: " + ipCliente);//Impresion de mensaje en consola

				if (!url.endsWith(".html"))//Aniadimos la extencion del archivo (solo si esta no la especificaron desde el navegador)
					url += ".html";
				
				if (url.equals("favicon.ico.html") || url.endsWith("estilo8.css.html"))//Excepcion que controla cuando es requerida una pagina que utiliza css o esta es recargada
					url = ultimaUrl;//Utilizamos la utlima url valida 
				else
					ultimaUrl = url;//Guardamos el valor de la ultima url aceptada

				if (!new File(url).exists())//Evaluamos si el archivo requerido existe, de lo contrario la pagina enviada sera  la de un mensaje de error
					url = "notfound.html";//Pagina de mensaje de error

				System.out.println("Pagina entregada: "+ url);// Impresion de mensaje en consola				
				
				FileInputStream fis = new FileInputStream(new File(url));//Creamos un objeto desde el cual podremos obtener la secuencia de bytes del archivo				
				byte[] bytes = fis.readAllBytes() ;//Arreglo de bytes donde almacenaremos el archivo para posteriormente enviarlo;

				OutputStream flujosalida = socketEntrada.getOutputStream();//Definimos el canal de salida (el mismo socket desde donde recibimos la peticion)
				flujosalida.write(bytes, 0, bytes.length);// Almacenamos la secuencia de bytes dentro del flujo de salida
				flujosalida.flush();// confirmamos la escritura del archivo
							
				flujosalida.close();// Cerramos el flujo de salida
			} catch (Exception ex) {
				// Capturamos cualquier tipo de excepcion que se pudiese producir
			}
			socketEntrada.close();// Cerramos el socket de entrada
			flujoEntrada.close();// Cerramos el flujo de entrada

			System.out.println("Proceso exitoso\n");// Impresion de mensaje en consola
		}
	}
}