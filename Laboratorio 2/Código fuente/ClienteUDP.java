// Librerias utilizadas en la clase
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

class Server extends Thread implements Initializable {

    @FXML private TextArea textFieldRespuestaServer; //textarea para visualizar la respuesta del servidor

    @Override
    public void run() {
        try {
            Thread.sleep(1000); // se arregla el desfase de conexión esperando 1 segundo
            Random random = new Random(); // se crea un objeto de la clase Random
            DatagramSocket socketUDP = new DatagramSocket(6789); // se crea un socket servidor indicando el puerto de conexión abierto
            byte[] buffer = new byte[100]; // se crea un buffer y se indica la cantidad maxima de caracteres a recibir por el servidor

            while (true) { // se ejecuta el servidor
                    int randomNum = random.nextInt(11); // genera un número aleatorio en el rango 0 - 10

                    DatagramPacket peticion = new DatagramPacket(buffer, buffer.length); // se crea un socket en donde se almacenan los datos del cliente
                    socketUDP.receive(peticion); // se recibe y se guarda la peticion del cliente

                    DatagramPacket respuesta = new DatagramPacket(randomNum <= 5
                            ? new String(peticion.getData()).toUpperCase().getBytes() : "......".getBytes(), peticion.getLength(),
                            peticion.getAddress(), peticion.getPort()); // si el numero aleatorio es menor a 6 se simula que el paquete se pierde

                    socketUDP.send(respuesta); // se envía la respuesta al cliente
                    if (randomNum <= 5) { // si el paquete no se perdió
                        textFieldRespuestaServer.appendText("Ping Received of " + peticion.getAddress().getHostAddress() + "\n"); //se muestra la ip del cliente
                    }

            }
        } catch (Exception e) {
        } 
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        start(); // se inicia el hilo que ejecuta el servidor
    }
}