// Librerias utilizadas en la clase
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.StringTokenizer;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyEvent;

class Client {

    @FXML private JFXTextField textFieldMensaje; // textfield en donde se escribir� el mensaje a enviar
    @FXML private TextArea textFieldRespuestaServer; // textarea en donde aparecer�n los mensajes enviados por el servidor

    @FXML
    void onKeyTextFieldMessage(KeyEvent event) {
        if (event.getCode().ENTER.equals(event.getCode())) {

            StringTokenizer token = new StringTokenizer(textFieldMensaje.getText()); // se crea un token para separar el numSecuencia del mensaje
            String message = token.nextToken(); // se almancena el mensaje a enviar
            int cantPings = Integer.parseInt(token.nextToken()), numSecuencia = 1; // se almacena la cantidad de pings a realizar 

            while (cantPings-- > 0) {
                try {
                    textFieldMensaje.setText("");

                    long inicio = System.currentTimeMillis(); // se empieza a realizar el conteo de env�o del mensaje
                    DatagramSocket socketUDP = new DatagramSocket(); // Se crea un DatagramSocket (UDP)
                    byte[] mensaje = message.getBytes(); // se convierte el string a enviar en una secuencia de bytes
                    InetAddress hostServidor = InetAddress.getByName("172.30.10.61"); // se especifica la ip del servidor
                    int puertoServidor = 6789; // se especifica el puerto de conexi�n con el servidor
                    // se crea un socket para enviarselo al sevidor
                    DatagramPacket peticion = new DatagramPacket(mensaje, mensaje.length, hostServidor, puertoServidor);
                    socketUDP.send(peticion); // se env�a el socket al servidor
                    long fin = System.currentTimeMillis() - inicio; // se calcula el tiempo de duraci�n del env�o del mensaje

                    byte[] buffer = new byte[100000]; // se crea un buffer indicando el tama�o maximo del mensaje recibido
                    DatagramPacket respuesta = new DatagramPacket(buffer, buffer.length); // se crea un socket para almacenar la respuesta del servidor
                    socketUDP.receive(respuesta); // se recibe la respuesta del servidor

                    String resp = new String(respuesta.getData()); // se convierte de bytes a string el mensaje
                    //se muestra el mensaje en el text field
                    textFieldRespuestaServer.appendText((!resp.startsWith(".")) ? "Ping " + (numSecuencia) + " " + (fin / 1000.0) + " segundos. MESSAGE: " + message.toUpperCase() + "\n" : "TIMEOUT" + "\n");
                    numSecuencia++; // incrementa en 1 los pings realizados

                    socketUDP.close(); // se cierra el socket de la conexi�n

                } catch (IOException e) {
                }

            }
        }

    }
}